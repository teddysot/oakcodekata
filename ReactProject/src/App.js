import React, { useState } from 'react'
import './App.css';

function App() {
  return (
    <div className="App">
      <CardList />
    </div>
  );
}

function Card(props) {
  return (
    <div className="card">
      <img className="card-img-top" src={props.featureImage} alt="cap image" />
      <div className="card-body">
        <h5 className="card-title">{props.title}</h5>
        <p className="card-text">{props.description}</p>
        <a href={props.link} className="btn btn-primary">Learn more</a>
      </div>
    </div>
  );
}
function CardList() {
  return (
    <div className="row">
      <div className="col-sm-4">
        <Card featureImage="https://sebhastian.com/static/eb0e936c0ef42ded5c6b8140ece37d3e/fcc29/feature-image.png"
          title="How To Make Interactive ReactJS Form"
          description="Let's write some interactive form with React"
          link="https://sebhastian.com/interactive-react-form"
        />
      </div>
      <div className="col-sm-4">
        <Card
        // your data
        />
      </div>
      <div className="col-sm-4">
        <Card
        // your data
        />
      </div>
    </div>
  );
}

function Step1(props) {
  if (props.currentStep !== 1) {
    return null
  }
  return (
    <div className="form-group">
      <label htmlFor="email">Email address</label>
      <input
        className="form-control"
        id="email"
        name="email"
        type="text"
        placeholder="Enter email"
        value={props.email} // props from parent
        onChange={props.handleChange} // props from parent
      />
    </div>
  )
}

function MasterForm(props) {
  const [currentStep, setCurrentStep] = useState(1)
  const [email, setEmail] = useState('')
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')

  const handleChange = event => {
    const { name, value } = event.target
    this.setState({
      [name]: value
    })
  }

  const handleSubmit = event => {
    event.preventDefault()
    const { email, username, password } = this.state
    alert(`Your registration detail: \n 
           Email: ${email} \n 
           Username: ${username} \n
           Password: ${password}`)
  }

  _next = () => {
    let currentStep = this.state.currentStep
    currentStep = currentStep >= 2 ? 3 : currentStep + 1
    this.setState({
      currentStep: currentStep
    })
  }

  _prev = () => {
    let currentStep = this.state.currentStep
    currentStep = currentStep <= 1 ? 1 : currentStep - 1
    this.setState({
      currentStep: currentStep
    })
  }

  render(
    <>
      <h1>A Wizard Form!</h1>
      <p>Step {this.state.currentStep} </p>

      <form onSubmit={this.handleSubmit}>
        <Step1
          currentStep={this.state.currentStep}
          handleChange={this.handleChange}
          email={this.state.email}
        />
        <Step2
          currentStep={this.state.currentStep}
          handleChange={this.handleChange}
          username={this.state.username}
        />
        <Step3
          currentStep={this.state.currentStep}
          handleChange={this.handleChange}
          password={this.state.password}
        />
      </form>
    </>
  )
}

export default App;
